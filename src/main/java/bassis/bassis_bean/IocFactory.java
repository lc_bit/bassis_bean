package bassis.bassis_bean;

import java.util.Map;

import bassis.bassis_bean.annotation.impl.AutowiredImpl;
import bassis.bassis_tools.exception.CustomException;

public class IocFactory {
	private static class LazyHolder {
		private static final IocFactory INSTANCE = new IocFactory();
	}

	private IocFactory() {
	}

	public static final IocFactory getInstance() {
		return LazyHolder.INSTANCE;
	}
	/**
	 * IOC
	 * 全字段注入，自动注入注解将被忽略
	 * @param clz
	 * @param args
	 */
	public static void IOC(Class<?> clz, Map<String, Object> mapRds) {
		try {
			//忽略自动注入注解
			AutowiredImpl.ignoreAutowired();
			AutowiredImpl.analyseFieldIOC(clz, mapRds);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			CustomException.throwOut("IOC failure ",e);
		}
	}
	/**
	 * IOC
	 * 只对自动注入注解
	 * @param clz
	 * @param args
	 */
	public static void analyseFieldIOC(Class<?> clz, Map<String, Object> mapRds) {
		try {
			//自动注入注解
			AutowiredImpl.enableAutowired();
			AutowiredImpl.analyseFieldIOC(clz, mapRds);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			CustomException.throwOut("IOC failure ",e);
		}
	}
}
